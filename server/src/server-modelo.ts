import express from 'express';

const app = express();

app.use(express.json());

const users = ['Rondy','Ana','Marcos','Joao'];

app.get('/users', (request, response) => {
  console.log('Listagem de usuarios');

  const search = String(request.query.search);

  const filteredUsers = search ? users.filter(user => user.includes(search)) : users;

  console.log(filteredUsers);

  return response.json(filteredUsers);

});

app.get('/users/:id', (request, response) => {
  console.log('Listagem de usuario');

  const id = Number(request.params.id);

  const user = users[id];

  return response.json(user);

});

 app.post('/users', (request, response) => {
    console.log('Cria usuario');

    const data = request.body;
    console.log(data);

    const user = {
      nome: data.name,
      email: data.email
    }

    return response.json(user);

});

app.listen(3333);