import Knex from 'knex';

export async function up(knex: Knex){
    return knex.schema.createTable('points', tables =>{
        tables.increments('id').primary();
        tables.string('image').notNullable;
        tables.string('name').notNullable;
        tables.string('email').notNullable;
        tables.string('whatsapp').notNullable;
        tables.decimal('latitude').notNullable;
        tables.decimal('longitude').notNullable;
        tables.string('city').notNullable;
        tables.string('uf', 2).notNullable;
    });
}

export async function down(knex: Knex){
    return knex.schema.dropTable('points');
}