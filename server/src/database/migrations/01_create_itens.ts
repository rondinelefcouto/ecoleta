import Knex from 'knex';

export async function up(knex: Knex){
    return knex.schema.createTable('itens', tables =>{
        tables.increments('id').primary();
        tables.string('title').notNullable;        
        tables.string('image').notNullable;
    });
}

export async function down(knex: Knex){
    return knex.schema.dropTable('itens');
}