import Knex from 'knex';

export async function up(knex: Knex){
    return knex.schema.createTable('points_itens', tables =>{
        tables.increments('id').primary();
        tables.integer('point_id').notNullable().references('id').inTable('points');
        tables.integer('item_id').notNullable().references('id').inTable('itens');;
    });
}

export async function down(knex: Knex){
    return knex.schema.dropTable('points');
}