import Knex from 'knex';

export async function seed(knex: Knex){
    await knex('itens').insert([
        { title: 'Lâmpadas', image: 'lampadas.svg'},
        { title: 'Pilhas e Baterias', image: 'lampadas.svg'},
        { title: 'Papeis e Papelão', image: 'lampadas.svg'},
        { title: 'Resíduos Eletrônicos', image: 'lampadas.svg'},
        { title: 'Resíduos Organicos', image: 'lampadas.svg'},
        { title: 'Óleo de Cozinha', image: 'lampadas.svg'},
    ]);
}