import React, { useEffect, useState, ChangeEvent } from 'react';
import { FiArrowLeft }  from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { Map, TileLayer, Marker,Popup } from "react-leaflet";
import { LeafletMouseEvent } from 'leaflet';
import axios from "axios";

import api from '../../services/api';

import "./style.css";

import logo from '../../assets/logo.svg';

interface Item{
    id: number;
    title: string;
    image:string;
}

interface UfIBGE{
    sigla: string;
}

interface CityIBGE{
    id: number;
    nome: string;
}


const CreatePoint = () => {

    const [itens, setItens] = useState<Item[]>([]);
    const [ufs, setUfs] = useState<string[]>([]);
    const [selectedUF, setselectedUF] = useState('0');

    const [cities, setCities] = useState<string[]>([]);
    const [selectedCity, setselectedCity] = useState('0');
    
    const [selectedPosition, setselectedPosition] = useState<[number,number]>([0,0]);
    const [initialPosition, setinitialPosition] = useState<[number,number]>([0,0]);

    useEffect(()=>{
        api.get('itens').then(response => {
            setItens(response.data);
        });
    },[]);


    useEffect(()=>{
        axios.get<UfIBGE[]>('https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome').then(response => {
            const ufInitials = response.data.map(uf => uf.sigla);
            setUfs(ufInitials);
        });
    },[]);  
    
    useEffect(()=>{
        if(selectedUF === '0'){
            return;
        }

        axios.get<CityIBGE[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUF}/distritos`).then(response => {
            const cityNames = response.data.map(city => city.nome);
            setCities(cityNames);
        });

    },[selectedUF]);  
    
    useEffect(()=>{
            navigator.geolocation.getCurrentPosition(position => {
                const { latitude, longitude } = position.coords;
                setinitialPosition([latitude,longitude]);
            });
    },[]);      

    function handleSelectUF(event: ChangeEvent<HTMLSelectElement>){
        const uf = event.target.value;
        setselectedUF(uf);
    }

    function handleSelectCity(event: ChangeEvent<HTMLSelectElement>){
        const city = event.target.value;
        setselectedCity(city);
    }    

    function handleMapClick(event: LeafletMouseEvent){
        
        setselectedPosition([event.latlng.lat,event.latlng.lng]);
    } 
    
    function handleInptChange(event: ChangeEvent<HTMLInputElement>){
        console.log(event);
        
    }     

    return (
        <div id="page-create-point">
            <header>
                <img src={logo} alt="Ecoleta" />

                <Link to="/">
                    <FiArrowLeft />
                    voltar para home
                </Link>

            </header>

            <form>
                <h1>Cadastro do <br /> Ponto de Coleta</h1>

                <fieldset>
                    <legend>
                        <h2>Dados</h2>
                    </legend>
                    <div className="field">
                        <label htmlFor="name">Nome da Entidade</label>
                        <input type="text" name="name" id="name" onChange={handleInptChange}/>
                    </div>
                    <div className="field-group">
                        <div className="field">
                            <label htmlFor="email">email</label>
                            <input type="email" name="email" id="email" onChange={handleInptChange}/>
                        </div>
                        <div className="field">
                            <label htmlFor="whatsapp">whatsapp</label>
                            <input type="text" name="whatsapp" id="whatsapp" onChange={handleInptChange}/>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>
                        <h2>Endereço</h2>
                        <span>Selecione o endereço no mapa</span>
                    </legend>

                    <Map center={initialPosition} zoom={15} onClick={handleMapClick}>
                        
                        <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />

                        <Marker position={selectedPosition} >
                            <Popup>
                            A pretty CSS3 popup. <br /> Easily customizable.
                            </Popup>
                        </Marker>

                    </Map>                    

                    <div className="field-group">
                        <div className="field">
                            <label htmlFor="uf">Estado</label>
                            <select name="uf" id="uf" value={selectedUF} onChange={handleSelectUF}>
                                <option value="">Selecione uma UF</option>
                                { ufs.map( uf => (
                                    <option key={uf} value={uf}>{uf}</option>
                                ))}
                            </select>
                        </div>
                        <div className="field">
                            <label htmlFor="city">Cidade</label>
                            <select name="city" id="city" value={selectedCity} onChange={handleSelectCity}>
                                <option value="">Selecione uma Cidade</option>
                                { cities.map( city => (
                                    <option key={city} value={city}>{city}</option>
                                ))}                                
                            </select>
                        </div>
                    </div>

                </fieldset>

                <fieldset>
                    <legend>
                        <h2>Ítens de Coleta</h2>
                        <span>Selecione um ou mais itens abaixo</span>
                    </legend>

                    <ul className="items-grid">
                        { itens.map(item => (
                            <li key={item.id}>
                                <img src={item.image} width="30" alt={item.title}/>
                                <span>{item.title}</span>
                            </li>
                        ))}                      
                    </ul>

                </fieldset>                

                <button type="submit">Cadastrar ponto de coleta</button>

            </form>
        </div>
    );
}

export default CreatePoint;