import React from 'react';

interface HeadersProps {
    title: String;
    title2?: String;
}

const Header: React.FC<HeadersProps> = (props) => {
    return (
        <header>
            <h1>{props.title}</h1>
        </header>
    );
}

export default Header;